import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import * as httpProxy from 'http-proxy-middleware'
import { GatewayService } from './gateway.service'
import { MorganMiddleware } from '@nest-middlewares/morgan'

@Module({
  imports: [],
  controllers: [],
  providers: [GatewayService]
})
export class GatewayModule implements NestModule {
  constructor(private readonly gatewayService: GatewayService) {}

  public configure(consumer: MiddlewareConsumer) {
    this.gatewayService.getJsonRoutes().map(opt => {
      const { path, ...routeOptions } = opt
      const proxyOptions = {
        ...this.gatewayService.getDefaultOptions(),
        ...routeOptions
      }
      consumer.apply(httpProxy(path, proxyOptions)).forRoutes('*')

      MorganMiddleware.configure('common')
      consumer.apply(MorganMiddleware).forRoutes('*')
    })
  }
}
