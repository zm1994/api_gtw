import * as httpProxy from 'http-proxy-middleware'

interface IProxy extends httpProxy.Config {
  path: string[]
}

export interface IGatewayService {
  getDefaultOptions(): any
  getJsonRoutes(): IProxy[]
}
