import { Injectable } from '@nestjs/common'
import { IGatewayService } from './interfaces'
import routes from '../utils/routes'

@Injectable()
export class GatewayService implements IGatewayService {
  public getDefaultOptions(): object {
    return {
      logLevel: 'debug',
      changeOrigin: true,
      prependPath: true
    }
  }

  public getJsonRoutes(): any {
    return routes
  }
}
