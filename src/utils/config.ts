export const apiPort = process.env.API_PORT || 5000
export const paymentHostAPI =
  process.env.PAYMENT_API_HOST || 'http://localhost:3001'
export const subscriptionHostAPI =
  process.env.SUBSCRIPTION_API_HOST || 'http://localhost:3002'
