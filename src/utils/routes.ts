import { paymentHostAPI, subscriptionHostAPI } from './config'

const apiVersionPref = '/api/v1'
const customersApi = `${apiVersionPref}/customers`

const routes = [
  {
    path: customersApi,
    target: paymentHostAPI
  },
  {
    path: [
      `${customersApi}/*`,
      `${customersApi}/*/cards/**`,
      `${customersApi}/*/payments/**`
    ],
    target: paymentHostAPI
  },
  {
    path: `${customersApi}/*/subscriptions/**`,
    target: subscriptionHostAPI
  }
]

export default routes
