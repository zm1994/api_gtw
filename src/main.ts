import { NestFactory } from '@nestjs/core'
import { AppModule } from './app/app.module'
import * as config from './utils/config'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })
  await app.listen(config.apiPort)
}
bootstrap()
